PROG :=		verify-scheme

CSC :=		csc
CSC_FLAGS = 	-O3

# BUILD_DIR :=	./build
# SRC_DIRS :=	./src

all:		$(PROG)

$(PROG):	$(PROG).scm
	$(CSC) $(CSC_FLAGS) $(PROG).scm -o $@

.PHONY:	clean
clean:
	rm -f $(PROG)

run:		$(PROG)
	@./$(PROG)
